import { UsersByRoleRepository } from "../repositories/users-by-role.repository";
import { User } from "../models/user.model";

export class UsersByRoleProjector {
  constructor(private usersByRoleRepository: UsersByRoleRepository) {}

  projectNewUser(user: User) {
    this.usersByRoleRepository.increaseCount(user.role);
  }
}
