import { Command } from "../../../shared/command-bus";

export const CREATE_USER_COMMAND = "users/CREATE_USER";

export interface CreateUserCommandPayload {
  id: string;
  username: string;
  password: string;
  role: string;
}

export class CreateUserCommand implements Command<CreateUserCommandPayload> {
  public type: string = CREATE_USER_COMMAND;

  constructor(public payload: CreateUserCommandPayload) {}
}
