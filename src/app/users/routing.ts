import * as express from "express";
import { CommandBus } from "../../shared/command-bus";

import { createUserAction } from "./actions/create-user.action";
import { getUsersRolesAction } from "./actions/get-users-roles.action";
import { UsersByRoleRepository } from "./repositories/users-by-role.repository";
// COMMAND_IMPORTS

export interface UsersRoutingProps {
  commandBus: CommandBus;
  usersByRoleRepository: UsersByRoleRepository;
}

// eslint-disable-next-line
export const usersRouting = ({ commandBus, usersByRoleRepository }: UsersRoutingProps) => {
  const router = express.Router();

  router.post("/", createUserAction({ commandBus }));
  router.get("/roles", getUsersRolesAction({ usersByRoleRepository }));
  // COMMANDS_SETUP

  return router;
};
