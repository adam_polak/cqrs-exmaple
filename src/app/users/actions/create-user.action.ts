import { Request, Response, NextFunction } from "express";
import { v4 } from "uuid";
import { CommandBus } from "../../../shared/command-bus";
import { CreateUserCommand } from "../commands/create-user.command";

export interface CreateUserActionProps {
  commandBus: CommandBus;
}

export const createUserAction = ({ commandBus }: CreateUserActionProps) => (
  req: Request,
  res: Response,
  next: NextFunction,
) => {
  commandBus
    .execute(
      new CreateUserCommand({
        id: v4(),
        username: req.body.username,
        password: req.body.password,
        role: req.body.role,
      }),
    )
    .then(commandResult => {
      res.json(commandResult);
    })
    .catch(next);
};
