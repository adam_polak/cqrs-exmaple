import { Request, Response } from "express";
import { UsersByRoleRepository } from "../repositories/users-by-role.repository";

export interface GetUsersRolesActionProps {
  usersByRoleRepository: UsersByRoleRepository;
}

export const getUsersRolesAction = ({ usersByRoleRepository }: GetUsersRolesActionProps) => (
  req: Request,
  res: Response,
) => {
  const usersByRole = usersByRoleRepository.getAll();

  res.json(usersByRole);
};
