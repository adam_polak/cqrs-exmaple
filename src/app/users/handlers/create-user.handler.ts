import { Handler } from "../../../shared/command-bus/index";
import { CreateUserCommand, CREATE_USER_COMMAND } from "../commands/create-user.command";
import { UsersRepository } from "../repositories/users.repository";
import { User } from "../models/user.model";
import { UsersByRoleProjector } from "../projectors/users-by-role.projector";

export class CreateUserHandler implements Handler<CreateUserCommand> {
  public commandType = CREATE_USER_COMMAND;

  constructor(private usersRepository: UsersRepository, private usersByRoleProjector: UsersByRoleProjector) {}

  async execute(command: CreateUserCommand) {
    const user = new User(command.payload.id, command.payload.username, command.payload.password, command.payload.role);

    await this.usersRepository.insert(user);
    await this.usersByRoleProjector.projectNewUser(user);
  }
}
