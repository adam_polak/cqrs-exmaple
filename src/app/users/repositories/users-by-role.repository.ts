interface UsersByRole {
  [role: string]: number;
}

export class UsersByRoleRepository {
  constructor(private usersByRole: UsersByRole = {}) {}

  increaseCount(role: string) {
    if (this.usersByRole[role]) {
      this.usersByRole[role] += 1;
    } else {
      this.usersByRole[role] = 1;
    }
  }

  decreaseCount(role: string) {
    if (this.usersByRole[role]) {
      this.usersByRole[role] -= 1;
    }
  }

  getAll() {
    return this.usersByRole;
  }
}
