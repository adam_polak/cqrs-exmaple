import { User } from "../models/user.model";

export class UsersRepository {
  constructor(private users: User[] = []) {}

  insert(user: User) {
    this.users.push(user);
  }

  find(criteria: (value: User, index: number, array: User[]) => User[]) {
    return this.users.filter(criteria);
  }
}
