import * as express from "express";
import * as helmet from "helmet";
import * as cors from "cors";
import { createRouter } from "./router";
import { usersRouting } from "./users/routing";
import { CommandBus } from "../shared/command-bus";
import { UsersByRoleRepository } from "./users/repositories/users-by-role.repository";

function createApp(commandBus: CommandBus, usersByRoleRepository: UsersByRoleRepository) {
  const app = express();

  app.use(cors());
  app.use(helmet());
  app.use(express.json());

  app.get("/health", (req, res) => {
    res.status(200).json({
      status: "200 - ok",
    });
  });

  app.use(
    "/api",
    createRouter({
      usersRouting: usersRouting({ commandBus, usersByRoleRepository }),
    }),
  );

  return app;
}

export { createApp };
