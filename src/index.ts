import { createApp } from "./app/app";
import { CommandBus } from "./shared/command-bus";
import { UsersByRoleRepository } from "./app/users/repositories/users-by-role.repository";
import { UsersRepository } from "./app/users/repositories/users.repository";
import { CreateUserHandler } from "./app/users/handlers/create-user.handler";
import { UsersByRoleProjector } from "./app/users/projectors/users-by-role.projector";

(async () => {
  process.on("uncaughtException", err => {
    // eslint-disable-next-line
    console.log(`Uncaught: ${err.toString()}`, err);
    process.exit(1);
  });

  process.on("unhandledRejection", err => {
    if (err) {
      // eslint-disable-next-line
      console.log(`Uncaught: ${err.toString()}`, err);
    }
    process.exit(1);
  });

  const usersByRoleRepository = new UsersByRoleRepository();
  const usersRepository = new UsersRepository();
  const usersByRoleProjector = new UsersByRoleProjector(usersByRoleRepository);

  const commandBus = new CommandBus([new CreateUserHandler(usersRepository, usersByRoleProjector)]);

  const app = createApp(commandBus, usersByRoleRepository);
  app.listen(3000);
})();
